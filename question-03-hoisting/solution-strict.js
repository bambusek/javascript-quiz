'use strict';

(function () {
    console.log(bar);
    console.log(baz);

    foo();
    function foo() {

    }

    var bar = 1;
    baz = 2; // strict mode prohibits assignment to undeclared variable => ReferenceError
}())
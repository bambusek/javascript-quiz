(function () {
    console.log(bar); // declared variables are hoisted => undefined
    console.log(baz); // baz has no declaration that could be hoisted => ReferenceError: baz is not defined

    foo(); // declared functions are also hoisted
    function foo() {

    }

    var bar = 1;
    baz = 2;
}())
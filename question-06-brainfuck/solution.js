console.log(!!-1-1); // 0
/*
(!(!-1))-1 : !-1 invokes coercion on -1 by internal ToBoolean which returns false for +0,-0 and NaN, true for others
(!(!true)-1 : !true negates the true
(!false)-1 : !false negates the false
true-1 : true is coerced by ToNumber to 1
1-1 => 0
*/
var obj = {
    method: function () {
        console.log(this);
    }
}

var func = obj.method;
func(); // 'this' is bind to the calling place - here it will return global object
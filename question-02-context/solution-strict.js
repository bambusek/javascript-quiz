'use strict';

var obj = {
    method: function () {
        console.log(this);
    }
}

var func = obj.method;
func(); // in strict mode, this remains at whatever it was set to when entering the execution context => undefined
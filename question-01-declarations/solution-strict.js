'use strict';

(function () {
    var bar = 5;
    baz = 4; // strict mode prohibits assignment of undeclared variables => ReferenceError: baz is not defined
}())

console.log(bar);
console.log(baz);
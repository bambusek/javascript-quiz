(function () {
    var bar = 5; // creates local variable
    baz = 4; // assignment of undeclared variable tries to find the declaration in upper scopes, if it doesn't find it in some scope, it creates global variable
}())

console.log(bar); // Reference error: bar is not defined
console.log(baz); // 4
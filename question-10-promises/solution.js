var doSomething = function () { return Promise.resolve('doSomething'); };
var doSomethingElse = function () { return Promise.resolve('doSomethingElse'); };
var finalHandler = function (value) { console.log(value); };


doSomething().then(function () {
    return doSomethingElse();
}).then(finalHandler); // 'doSomethingElse'

doSomething().then(function () {
    doSomethingElse();
}).then(finalHandler); // undefined

doSomething().then(doSomethingElse).then(finalHandler); // doSomethingElse


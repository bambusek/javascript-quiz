var doSomething = function () {return Promise.resolve('doSomething');};
var doSomethingElse = function () {return Promise.resolve('doSomethingElse');};
var finalHandler = function (value) {console.log(value);};


doSomething().then(function () {
    return doSomethingElse();
}).then(finalHandler);

doSomething().then(function () {
    doSomethingElse();
}).then(finalHandler);

doSomething().then(doSomethingElse).then(finalHandler);


'use strict';

(function () {
    var a = b = 5; // ReferenceError: b is not defined - strict mode prohibits assignment of undeclared variables
})();

console.log(b);
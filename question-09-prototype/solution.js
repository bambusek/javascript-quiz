// Define a repeatify function on the String object. The function accepts an integer that specifies how many times the string has to be repeated. The function returns the string repeated the number of times specified. For example console.log('hello'.repeatify(3)) should print 'hellohellohello'

String.prototype.repeatify = function (count) {
    return count ? this + this.repeatify(count-1) : this;
}

console.log('hello'.repeatify(3)); // Should print 'hellohellohello'


# Javascript Quiz

* Q1 - declarations
* Q2 - context
* Q3 - hoisting
* Q4 - closure
* Q5 - string manipulation
* Q6 - brainfuck - coercion and operator precedence
* Q7 - associativity
* Q8 - coercion
* Q9 - prototype
* Q10 - promises

function createFunctions() {
    var result = [];

    for (var i = 0; i < 10; i++) {
        result[i] = function () {
            return i;
        }
    }

    return result;
}

createFunctions().forEach(function (item) {
    console.log(item()); // prints 10 times 10, because i in return is tied to its closure, that is 10 at the time of calling the functions
});

/* 
How to rewrite it to print 1,2,...,9 ?

function createFunctions() {
    var result = [];
    for (var i = 0; i < 10; i++) {
        (function(){
            var j = i;
            result[j] = function () {
                return j;
            }
        })();
    }

    return result;
}

*/
function createFunctions () {
    var result = [];

    for (var i = 0; i < 10; i++){
        result[i] = function() {
            return i;
        }
    }

    return result;
}

createFunctions().forEach(function (item) {
    console.log(item());
});